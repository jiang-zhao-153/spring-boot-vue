package com.x.controller;

import com.x.model.Category;
import com.x.model.CategoryQuery;
import com.x.model.Product;
import com.x.model.ProductQuery;
import com.x.service.CategoryService;
import com.x.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/product") //restFul
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping   //http://a.com?id=1&name=张三
    public List<Product> select(ProductQuery query) {
       return productService.selct(query);
    }
    @PostMapping
    public int insert(@RequestBody Product model) {
        return productService.insert(model);
    }
    @PutMapping
    public int update(@RequestBody Product model) {
        return productService.update(model);
    }
    @DeleteMapping  //http://a.com?id=1&name=张三
    public int delete(Integer id) {
        return productService.delete(id);
    }
}
