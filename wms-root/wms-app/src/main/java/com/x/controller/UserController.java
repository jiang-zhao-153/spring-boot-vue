package com.x.controller;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.x.model.FileInfo;
import com.x.model.LoginInfo;
import com.x.model.RegisterInfo;
import com.x.model.UploadInfo;
import com.x.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/user")
public class UserController {


    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public String register(@RequestBody RegisterInfo registerInfo) {
       return userService.register(registerInfo);
    }
    @PostMapping("/login")
    public String login(@RequestBody LoginInfo loginInfo) {
        return userService.login(loginInfo);
    }

}
