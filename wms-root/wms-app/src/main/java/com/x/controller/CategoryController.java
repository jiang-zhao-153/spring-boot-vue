package com.x.controller;

import com.x.model.Category;
import com.x.model.CategoryQuery;
import com.x.model.LoginInfo;
import com.x.model.RegisterInfo;
import com.x.service.CategoryService;
import com.x.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/category") //restFul
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping   //http://a.com?id=1&name=张三
    public List<Category> select(CategoryQuery query) {
       return categoryService.selct(query);
    }
    @PostMapping
    public int insert(@RequestBody Category category) {
        return categoryService.insert(category);
    }
    @PutMapping
    public int update(@RequestBody Category category) {
        return categoryService.update(category);
    }
    @DeleteMapping  //http://a.com?id=1&name=张三
    public int delete(Integer id) {
        return categoryService.delete(id);
    }
}
