package com.x.service;

import com.x.dao.CategoryDao;
import com.x.dao.ProductDao;
import com.x.model.Category;
import com.x.model.CategoryQuery;
import com.x.model.Product;
import com.x.model.ProductQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductDao productDao;

    public List<Product> selct(ProductQuery query) {
        return productDao.select(query);
    }

    public int insert(Product product) {

        return productDao.insert(product);
    }

    public int update(Product product) {

        return productDao.update(product);
    }

    public int delete(Integer id) {
        return   productDao.delete(id);
    }
}
