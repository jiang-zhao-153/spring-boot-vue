package com.x.service;

import cn.hutool.core.util.ObjUtil;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.JWTUtil;
import cn.smart.exception.BizException;
import cn.smart.model.LocalUser;
import cn.smart.threadlocal.LocalUserUtil;
import com.x.dao.CategoryDao;
import com.x.dao.UserDao;
import com.x.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    public List<Category> selct(CategoryQuery query) {
        return categoryDao.select(query);
    }

    public int insert(Category category) {
        //校验
       // LocalUser user = LocalUserUtil.get();
       // category.setLastUpdateBy(user.getNickName());
        return categoryDao.insert(category);
    }

    public int update(Category category) {

        return categoryDao.update(category);
    }

    public int delete(Integer id) {
        return   categoryDao.delete(id);
    }
}
