package com.x.intercept;


import cn.smart.threadlocal.LocalUserUtil;
import com.x.model.BaseModel;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.util.Properties;

@Component
@Intercepts(@Signature(type= ParameterHandler.class,method="setParameters", args = {PreparedStatement.class}))
public class ParameterPlugin implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        ParameterHandler parameterHandler =  (ParameterHandler)invocation.getTarget();
        Object parameterObject = parameterHandler.getParameterObject();
        if(parameterObject instanceof BaseModel){
            BaseModel model = (BaseModel) parameterObject;
            model.setLastUpdateBy(LocalUserUtil.get().getNickName());
        }
        return invocation.proceed();
    }
    @Override
    public Object plugin(Object target) {
        return Interceptor.super.plugin(target);
    }
    @Override
    public void setProperties(Properties properties) {
    }
}
