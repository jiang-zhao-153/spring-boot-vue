package com.x.dao;

import com.x.model.RegisterInfo;
import com.x.model.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserDao {

    User select(String tel);
    User login(String tel,String password);
    int  insert(RegisterInfo registerInfo);
}
