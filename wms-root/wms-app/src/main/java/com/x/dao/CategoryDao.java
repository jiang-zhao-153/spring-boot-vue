package com.x.dao;


import cn.smart.annotation.PageX;
import com.x.model.Category;
import com.x.model.CategoryQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CategoryDao {
    @PageX
    List<Category> select(CategoryQuery query);
    int  insert(Category category);
    int  update(Category category);
    int  delete(int id);
}
