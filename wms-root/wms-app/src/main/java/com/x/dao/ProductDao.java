package com.x.dao;


import cn.smart.annotation.PageX;
import com.x.model.Category;
import com.x.model.CategoryQuery;
import com.x.model.Product;
import com.x.model.ProductQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProductDao {
    @PageX
    List<Product> select(ProductQuery query);
    int  insert(Product category);
    int  update(Product category);
    int  delete(int id);
}
