package com.x.enums;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Optional;

public enum CategoryStatus {

    SALE(1,"上架"),
    SHELVES(0,"下架");

    @Getter @Setter
    private Integer code;
    @Getter @Setter
    private String  msg;

    CategoryStatus(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public static CategoryStatus findByCode(Integer code){
        Optional<CategoryStatus> optional = Arrays.stream(CategoryStatus.values()).filter(item -> item.getCode().equals(code)).findFirst();
        return optional.orElse(null);
    }

}
