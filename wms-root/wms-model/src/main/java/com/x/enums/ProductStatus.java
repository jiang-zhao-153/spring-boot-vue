package com.x.enums;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Optional;

public enum ProductStatus {

    SALE(1,"上架"),
    SHELVES(0,"下架");

    @Getter @Setter
    private Integer code;
    @Getter @Setter
    private String  msg;

    ProductStatus(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public static ProductStatus findByCode(Integer code){
        Optional<ProductStatus> optional = Arrays.stream(ProductStatus.values()).filter(item -> item.getCode().equals(code)).findFirst();
        return optional.orElse(null);
    }

}
