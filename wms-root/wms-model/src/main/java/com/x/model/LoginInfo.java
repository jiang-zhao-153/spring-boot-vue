package com.x.model;

import lombok.Data;

@Data
public class LoginInfo {

    private String tel;
    private  String password;

}
