package com.x.model;

import lombok.Data;

@Data
public class RegisterInfo {

    private String tel;
    private  String nickName;
    private  String password;
    private  String email;
}
