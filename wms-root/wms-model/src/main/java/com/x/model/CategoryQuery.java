package com.x.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CategoryQuery {
    private Integer id;
    private Integer[] ids;
    private String name;
    private Integer parentId;
    private Integer status ;
}
