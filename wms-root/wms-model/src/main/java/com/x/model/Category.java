package com.x.model;

import com.x.enums.CategoryStatus;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Category extends BaseModel {
    private Integer id;
    private String name;
    private String img;
    private Integer seq;
    private Integer parentId;
    private Integer status ;

    private String statusX;

    public String getStatusX() {

        CategoryStatus categoryStatus = CategoryStatus.findByCode(this.getStatus());
        if(categoryStatus!=null){
            return categoryStatus.getMsg();
        }
        return "";
    }
}
