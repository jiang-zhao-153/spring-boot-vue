package com.x.model;

import com.x.enums.CategoryStatus;
import com.x.enums.ProductStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class Product extends BaseModel {
    private Integer id;
    private String name;
    private String subName;
    private String img;
    private Integer seq;
    private Integer categoryId;
    private Integer status ;
    private BigDecimal price;
    private String Tags;
    private String brief;
    private String statusX;

    public String getStatusX() {

        ProductStatus categoryStatus = ProductStatus.findByCode(this.getStatus());
        if(categoryStatus!=null){
            return categoryStatus.getMsg();
        }
        return "";
    }
}
