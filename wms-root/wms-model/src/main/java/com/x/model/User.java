package com.x.model;

import lombok.Data;

@Data
public class User {
    private Integer id;
    private String tel;
    private String nickName;
    private String password;
    private String email;
}
