package com.x.model;

import lombok.Data;

@Data
public class UploadInfo {

    private String name;
    private  String base64;
}
