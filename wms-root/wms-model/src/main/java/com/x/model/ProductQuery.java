package com.x.model;

import com.x.enums.ProductStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class ProductQuery extends BaseModel {
    private Integer id;
    private Integer[] ids;
    private String name;
    private Integer categoryId;
    private Integer status ;

}
