import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  server:{
    host:"0.0.0.0",
    port:5000,
    proxy:{
      "/api":{
        target:"http://localhost:8080/",
        changeOrigin:true
      }
    }
  },
  resolve:{
    //配置路径的一个别名
    alias:{
      '@': path.resolve(__dirname, './src'),
    }
  }
})
