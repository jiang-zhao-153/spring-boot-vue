import http from '@/http/index'


export default {
    register :{
        name:"注册用户",
        url:"/api/user/register",
        call: async function(params:any={}){
            return await http.post(this.url,params)
        }
    },
    login :{
        name:"用户登录",
        url:"/api/user/login",
        call: async function(params:any={}){
            return await http.post(this.url,params)
        }
    },
   

}