import http from '@/http/index'


export default {
    insert :{
        name:"商品新增",
        url:"/api/product",
        call: async function(params:any={}){
            return await http.post(this.url,params)
        }
    },
    select :{
        name:"商品查询",
        url:"/api/product",
        call: async function(params:any={}){
            return await http.get(this.url,params)
        }
    },
    update :{
        name:"商品修改",
        url:"/api/product",
        call: async function(params:any={}){
            return await http.put(this.url,params)
        }
    },
    delete :{
        name:"商品删除",
        url:"/api/product",
        call: async function(params:any={}){
            return await http.delete(this.url,params)
        }
    }

}