import http from '@/http/index'


export default {
    upload :{
        name:"上传文件",
        url:"/api/upload",
        call: async function(params:any={}){
            return await http.post(this.url,params)
        }
    },
    select :{
        name:"文件查询",
        url:"/api/upload",
        call: async function(params:any={}){
            return await http.get(this.url,params)
        }
    }
    

}