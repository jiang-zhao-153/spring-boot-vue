import http from '@/http/index'


export default {
    insert :{
        name:"品类新增",
        url:"/api/category",
        call: async function(params:any={}){
            return await http.post(this.url,params)
        }
    },
    select :{
        name:"品类查询",
        url:"/api/category",
        call: async function(params:any={}){
            return await http.get(this.url,params)
        }
    },
    update :{
        name:"品类修改",
        url:"/api/category",
        call: async function(params:any={}){
            return await http.put(this.url,params)
        }
    },
    delete :{
        name:"品类删除",
        url:"/api/category",
        call: async function(params:any={}){
            return await http.delete(this.url,params)
        }
    }

}