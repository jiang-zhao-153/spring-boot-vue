import productApi from "./productApi"
import uploadApi from "./uploadApi"
import userApi from "./userApi"
import categoryApi from "./categoryApi"
export {productApi,uploadApi,userApi,categoryApi}