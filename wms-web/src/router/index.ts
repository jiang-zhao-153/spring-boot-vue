import { createRouter, createWebHashHistory, createWebHistory } from "vue-router";
import useStore1 from '@/store/store1'


const routes = [
    {
        path: '/',
        name: "home", //以后跳转用的名称
        component: () => import("@/views/Home.vue"),
        meta: { title: '首页', icon: 'HomeFilled', show: true }
    },

    {
        path: '/user',
        name: 'user',
        component: () => import("@/views/user/Index.vue"),
        meta: { title: '用户管理', icon: 'Tickets', show: false, useFrame: false },
        children: [
            {
                path: '/user/login',
                name: 'user-login',
                component: () => import("@/views/user/Login.vue"),
                meta: { title: '登陆', icon: 'Menu', show: false, useFrame: false }
            },
            {
                path: '/user/register',
                name: 'user-register',
                component: () => import("@/views/user/Register.vue"),
                meta: { title: '注册', icon: 'Menu', show: false, useFrame: false }
            },

        ]
    },
    {
        path: '/category',
        name: 'category',
        component: () => import("@/views/category/Index.vue"),
        meta: { title: '品类管理', icon: 'Tickets' },
        children: [
            {
                path: '/category/list',
                name: 'category-list',
                component: () => import("@/views/category/List.vue"),
                meta: { title: '品类列表', icon: 'Memo' }
            },
            {
                path: '/category/add',
                name: 'category-add',
                component: () => import("@/views/category/Add.vue"),
                meta: { title: '添加品类', icon: 'CirclePlusFilled' }
            },
            {
                path: '/category/edit',
                name: 'category-edit',
                component: () => import("@/views/category/Edit.vue"),
                meta: { title: '修改品类', icon: 'CirclePlusFilled',show: false }
            }
        ]
    },
    {
        path: '/product',
        name: 'product',
        component: () => import("@/views/product/Index.vue"),
        meta: { title: '商品管理', icon: 'Tickets' },
        children: [
            {
                path: '/product/list',
                name: 'product-list',
                component: () => import("@/views/product/List.vue"),
                meta: { title: '商品列表', icon: 'Memo' }
            },
            {
                path: '/product/add',
                name: 'product-add',
                component: () => import("@/views/product/Add.vue"),
                meta: { title: '添加商品', icon: 'CirclePlusFilled' }
            },
            {
                path: '/product/edit',
                name: 'product-edit',
                component: () => import("@/views/product/Edit.vue"),
                meta: { title: '修改商品', icon: 'CirclePlusFilled',show: false  }
            }
        ]
    },


]

const router = createRouter({
    // history: createWebHashHistory('/'),
    history: createWebHistory(),
    routes
})
router.beforeEach((to, from, next) => {
    const store1 = useStore1()
    if (to.name != "user-login" && to.name != "user-register") {
        if (store1.token === "") {
            next('/user/login')
        } else {
            next()
        }
    }
    else {
        next()
    }
})

export default router