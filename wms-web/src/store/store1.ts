import { defineStore } from 'pinia'

 const useStore1 = defineStore('store1', {
  state: () => {
    return {
      token:"",
      user:{
        id:0,
        nickName:"游客",
        role:"",
        tel:"",
      },
      isMenuCollapse:false
    }
  },
  persist: {
    key: 'my-custom-key',
    storage: localStorage,
  },
})

export default useStore1